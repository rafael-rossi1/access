package br.com.itau.access.access.dtos;

import javax.validation.constraints.NotNull;

public class CreateAccessDto {


    @NotNull(message = "ID do cliente está nulo")
    private long customerIdDto;

    @NotNull(message = "ID da porta está nulo")
    private long doorIdDto;

    public CreateAccessDto() {
    }

    public long getCustomerIdDto() {
        return customerIdDto;
    }

    public void setCustomerIdDto(long customerIdDto) {
        this.customerIdDto = customerIdDto;
    }

    public long getDoorIdDto() {
        return doorIdDto;
    }

    public void setDoorIdDto(long doorIdDto) {
        this.doorIdDto = doorIdDto;
    }
}
