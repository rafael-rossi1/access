package br.com.itau.access.access.exceptions;

import org.springframework.http.HttpStatus;

public class AccessSystemException extends Exception {

    private HttpStatus httpStatus;

    AccessSystemException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
