package br.com.itau.access.access.dtos;

public class SearchAccessDto {


    private long customerIdDto;
    private long doorIdDto;

    public SearchAccessDto() {
    }

    public SearchAccessDto(long customerIdDto, long doorIdDto) {
        this.customerIdDto = customerIdDto;
        this.doorIdDto = doorIdDto;
    }

    public long getCustomerIdDto() {
        return customerIdDto;
    }

    public void setCustomerIdDto(long customerIdDto) {
        this.customerIdDto = customerIdDto;
    }

    public long getDoorIdDto() {
        return doorIdDto;
    }

    public void setDoorIdDto(long doorIdDto) {
        this.doorIdDto = doorIdDto;
    }
}


