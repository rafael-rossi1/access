package br.com.itau.access.access.exceptions;

import org.springframework.http.HttpStatus;

public class AccessNotFoundException extends AccessSystemException {
    public AccessNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }

}