package br.com.itau.access.access.controllers;


import br.com.itau.access.access.client.ZuulClient;
import br.com.itau.access.access.dtos.AccessMapper;
import br.com.itau.access.access.dtos.CreateAccessDto;
import br.com.itau.access.access.dtos.SearchAccessDto;
import br.com.itau.access.access.exceptions.AccessNotFoundException;
import br.com.itau.access.access.model.Access;
import br.com.itau.access.access.services.AccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/access")

public class AccessController {

    @Autowired
    private AccessService accessService;

    @Autowired
    private ZuulClient zuulClient;

    @Autowired
    private AccessMapper accessMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SearchAccessDto createAccess(@RequestBody @Valid CreateAccessDto createAccessDto) {
        zuulClient.getCustomer(createAccessDto.getCustomerIdDto());
        zuulClient.getDoor(createAccessDto.getDoorIdDto());
        try {
            Access access = accessMapper.toAccess(createAccessDto);
            Access accessDB = accessService.create(access);
            return accessMapper.toSearchAccessDto(access);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{clienteId}/{portaId}")
    public SearchAccessDto searchAccessByCustomerIdAndDoorId(@PathVariable(name = "customerId") long customerId,
                                                             @PathVariable(name = "doorId")     long doorId) throws AccessNotFoundException {
        Access access = accessService.searchByCustomerAndDoor(customerId, doorId);
        return accessMapper.toSearchAccessDto(access);
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAccessByCustomerIdAndDoorId(@PathVariable(name = "customerId") long customerId,
                                                  @PathVariable(name = "doorId")     long doorId) throws AccessNotFoundException {
        accessService.deleteAccessByCustomerIdAndDoorId(customerId, doorId);
    }


}
