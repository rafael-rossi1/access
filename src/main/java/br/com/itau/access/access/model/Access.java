package br.com.itau.access.access.model;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"doorId", "customerId"}))
@Entity

public class Access {

    private Long customerId;
    private Long doorId;

    public Access() {
    }

    public Access(Long customerId, Long doorId) {
    }

    public static Access anAccess() {
        return new Access();
    }

    public Access customerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public Access doorId(Long doorId) {
        this.doorId = doorId;
        return this;
    }

    public Access build() {
        return new Access(customerId, doorId);
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getDoorId() {
        return doorId;
    }

    public void setDoorId(Long doorId) {
        this.doorId = doorId;
    }
}
