package br.com.itau.access.access.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE, reason = "Clientes está indisponível.")
public class CustomerServiceDownException extends RuntimeException {
}