package br.com.itau.access.access.client;

import br.com.itau.access.access.exceptions.CustomerServiceDownException;
import br.com.itau.access.access.exceptions.DoorServiceDownException;

public class ServiceFallback implements ZuulClient {

    @Override
    public Customer getCustomer(Long id) {
        throw new CustomerServiceDownException();
    }

    @Override
    public Door getDoor(Long id) {
        throw new DoorServiceDownException();
    }
}
