package br.com.itau.access.access.dtos;


import br.com.itau.access.access.model.Access;
import org.springframework.stereotype.Component;

@Component
public class AccessMapper {

    public Access toAccess(CreateAccessDto createAccessDto) {
        Access access = new Access();
        access.setCustomerId(createAccessDto.getCustomerIdDto());
        access.setDoorId(createAccessDto.getDoorIdDto());
        return access;
    }

    public SearchAccessDto toSearchAccessDto(Access access) {
        SearchAccessDto searchAccessDto = new SearchAccessDto(access.getCustomerId(), access.getDoorId());
        return searchAccessDto;
    }
}
