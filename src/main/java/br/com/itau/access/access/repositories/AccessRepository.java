package br.com.itau.access.access.repositories;

import br.com.itau.access.access.model.Access;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccessRepository extends CrudRepository<Access, Long> {
    Optional<Access> findByCustomerIdAndDoorId(Long customerId, Long doorId);

    void deleteByCustomerIdAndDoorId(Long customerId, Long doorId);
}
