package br.com.itau.access.access.services;

import br.com.itau.access.access.exceptions.AccessNotFoundException;
import br.com.itau.access.access.model.Access;
import br.com.itau.access.access.repositories.AccessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;

public class AccessService {

    @Autowired
    AccessRepository accessRepository;

    public Access create(Access access) {
        try {
            return accessRepository.save(access);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Acesso já cadastrado no sistema.");
        }
    }
    public Access searchByCustomerAndDoor(long customerId, long doorId) throws AccessNotFoundException {
        Optional<Access> optionalAccess = accessRepository.findByCustomerIdAndDoorId(customerId, doorId);
        if(!optionalAccess.isPresent()) {
            throw new AccessNotFoundException(" Acesso não encontrado");
        }
        return optionalAccess.get();
    }

    public void deleteAccessByCustomerIdAndDoorId(long customerId, long doorId) throws AccessNotFoundException {
        Access access = searchByCustomerAndDoor(customerId, doorId);
        accessRepository.delete(access);
    }


}
