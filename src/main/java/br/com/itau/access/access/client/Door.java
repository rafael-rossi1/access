package br.com.itau.access.access.client;

public class Door {
    private Long id;
    private String floor;
    private String room;

    public Door(Long id, String floor, String room) {
        this.id = id;
        this.floor = floor;
        this.room = room;
    }

    public Long getId() {
        return id;
    }

    public String getFloor() {
        return floor;
    }

    public String getRoom() {
        return room;
    }
}
